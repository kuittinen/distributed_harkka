from django.contrib import admin
from store.models import Product
from store.models import User
from store.models import Order

# Register your models here.

admin.site.register(Product)
admin.site.register(User)

class OrderAdmin(admin.ModelAdmin):
    readonly_fields=('create_date',)

admin.site.register(Order, OrderAdmin)
