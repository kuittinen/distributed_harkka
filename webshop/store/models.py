from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False);
    info = models.CharField(max_length=100, blank=False, null=False);
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, blank=False, null=False)

    def __str__(self):
        return self.name

class Order(models.Model):
    username = models.CharField(max_length=100, blank=False, null=False);
    create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username

class User(AbstractUser):
    street = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    country = models.CharField(max_length=30)
