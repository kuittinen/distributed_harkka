from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views import generic
import datetime
from django.http import HttpResponse

from store.models import Product as Product_model
from store.models import Order
from store.forms import SignUpForm

# Create your views here.

# def index(request):
#     context = {}
#     return render(request, 'index.html', context)

class Store(generic.ListView):
    template_name = 'store.html'
    model = Product_model
    context_object_name = 'products'

class Product(generic.DetailView):
    template_name = 'product.html'
    model = Product_model

def Cart(request, product_id=None):
    sessionlist = request.session.get('cart_list', [])
    if product_id:
        sessionlist.append(product_id)
        request.session['cart_list'] = sessionlist

    products = []
    for i in sessionlist:
        products.append(Product_model.objects.get(pk=i))

    context = {
        'products': products
    }

    return render(request, 'cart.html', context)

def Checkout(request):
    order = Order.objects.create(username=request.user)
    request.session['cart_list'] = []
    return redirect('cart')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            country = form.cleaned_data['country']
            print(country)
            if country == 'Finland':
                f = form.save(commit=False)
                f.save(using='default')
            elif country == 'Sweden':
                f = form.save(commit=False)
                f.save(using='db1')
                print('sweden')
            elif country == 'Norway':
                f = form.save(commit=False)
                f.save(using='db3')
            else:
                print('Elsessa')
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('store')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})
