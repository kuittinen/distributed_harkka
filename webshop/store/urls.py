from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.Store.as_view(), name='store'),
    url(r'^product/(?P<pk>[0-9]+)/$', views.Product.as_view(), name='product'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'store'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^cart/$', views.Cart, name='cart'),
    url(r'^cart/(?P<product_id>[0-9]+)/$', views.Cart, name='cart'),
    url(r'^checkout/$', views.Checkout, name='checkout'),
]
