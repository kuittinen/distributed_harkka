from django import forms
from django.contrib.auth.forms import UserCreationForm
from store.models import User


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254)
    street = forms.CharField(max_length=30)
    city = forms.CharField(max_length=30)
    country = forms.CharField(max_length=30)

    class Meta:
        model = User
        fields = ('username', 'email', 'street', 'city', 'country', 'password1', 'password2', )
