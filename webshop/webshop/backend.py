# import the User object
from store.models import User

# Name my backend 'MyCustomBackend'
class CustomBackend:

    # Create an authentication method
    # This is called by the standard Django login procedure
    def authenticate(self, username=None, password=None):
        for db in ('db1', 'default', 'db3'):
            try:
                # Try to find a user matching your username
                user = User.objects.using(db).get(username=username)
                #  Check the password is the reverse of the username
                if username == user.username:
                    # Yes? return the Django user object
                    return user
                else:
                    # No? return None - triggers default login failed
                    print('ei ole.')
            except User.DoesNotExist:
                # No user was found, return None - triggers default login failed
                print('wrong database')
        return None

    # Required for your backend to work properly - unchanged in most scenarios
    def get_user(self, user_id):
        for db in ('db1', 'default', 'db3'):
            try:
                return User.objects.using(db).get(pk=user_id)
            except User.DoesNotExist:
                print('wrong database')
